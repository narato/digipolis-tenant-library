﻿using Microsoft.EntityFrameworkCore;
using Model = Digipolis.Common.Tenancy.Models;

namespace Digipolis.Tenant.Library.Models
{
    // Not an interface because we nee to ensure that (for example) saveChanges method exists
    public abstract class BaseTenantDbContext : DbContext
    {
        public DbSet<Model.Tenant> Tenants { get; set; }
        protected DbContextOptions _options;

        public BaseTenantDbContext()
        { }

        public BaseTenantDbContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }
    }
}