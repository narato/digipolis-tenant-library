﻿using Microsoft.EntityFrameworkCore;

namespace Digipolis.Tenant.Library.Models
{
    public partial class TenantDbContext : BaseTenantDbContext
    {
        private string DatabaseName = "Admin";
        private string _connectionString;

        public TenantDbContext(string connectionString)
        { 
            _connectionString = connectionString;
        }

        public TenantDbContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_options == null)
            {
                optionsBuilder.UseNpgsql(_connectionString);
            }
        }
    }
}