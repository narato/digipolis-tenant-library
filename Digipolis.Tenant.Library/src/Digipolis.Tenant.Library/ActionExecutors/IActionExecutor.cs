﻿using Digipolis.Common.Tenancy.MappedModels;

namespace Digipolis.Tenant.Library.ActionExecutors
{
    public interface IActionExecutor<ConfigT>
    {
        void OnTenantCreated(Tenant<ConfigT> tenant);
        void OnTenantDeleted(Tenant<ConfigT> tenant);
        void EnsureDbNamesAvailable(Tenant<ConfigT> tenant);
    }
}
