﻿using System;
using System.Linq;
using Model = Digipolis.Common.Tenancy.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Digipolis.Common.Status.Models;
using Digipolis.Tenant.Library.Models;

namespace Digipolis.Tenant.Library.DataProviders
{
    public class TenantDataProvider : ITenantDataProvider
    {
        private BaseTenantDbContext _tenantDbContext;
        public TenantDataProvider(BaseTenantDbContext tenantDbContext)
        {
            _tenantDbContext = tenantDbContext;
        }

        public DbConnectionCheckResult CheckDBConnection()
        {
            try
            {
                _tenantDbContext.Database.OpenConnection();
                _tenantDbContext.Database.ExecuteSqlCommand("SELECT N'Test'");
                _tenantDbContext.Database.CloseConnection();
            }
            catch (Exception ex)
            {
                return new DbConnectionCheckResult(false, ex);
            }
            return new DbConnectionCheckResult(true); 
        }

        public void EnsureTenantKeyAvailable(string key)
        {
            if (GetTenantByKey(key) != null)
                throw new ValidationException("Tenant with Key already exists: " + key);
        }

        public Model.Tenant GetTenantByKey(string key)
        {
            return _tenantDbContext.Tenants.SingleOrDefault(t => t.Key == key);
        }

        public Model.Tenant InsertTenant(Model.Tenant tenant)
        {
            var result = _tenantDbContext.Tenants.Add(tenant);
            _tenantDbContext.SaveChanges();
                
            return result.Entity;
        }
    }
}
