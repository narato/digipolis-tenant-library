﻿using Digipolis.Common.Status.Models;
using Model = Digipolis.Common.Tenancy.Models;

namespace Digipolis.Tenant.Library.DataProviders
{
    public interface ITenantDataProvider
    {
        Model.Tenant GetTenantByKey(string key);
        Model.Tenant InsertTenant(Model.Tenant tenant);
        DbConnectionCheckResult CheckDBConnection();
        void EnsureTenantKeyAvailable(string key);
    }
}
