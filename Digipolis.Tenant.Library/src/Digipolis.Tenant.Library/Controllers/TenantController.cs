﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Tenant.Library.Managers;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using Swashbuckle.SwaggerGen.Annotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Digipolis.Tenant.Library.Controllers
{
    [Route("api/tenants")]
    public class TenantController<ConfigT> : Controller
    {

        private ITenantManager<ConfigT> _tenantManager;
        private IResponseFactory _responseFactory;

        public TenantController(ITenantManager<ConfigT> tenantManager, IResponseFactory responseFactory)
        {
            _tenantManager = tenantManager;
            _responseFactory = responseFactory;
        }


        /// <summary>
        /// This method returns 1 tenant identified by its id 
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <returns>The tenant identified by the id for the given tenant key</returns>
        // GET api/tenant
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Request successful") ]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Tenant could not be found by the given key")]
        [HttpGet("{tenantkey}",Name = "GetTenantById")]
        public IActionResult Get([Required]string tenantKey)
        {
            return _responseFactory.CreateGetResponse(() => _tenantManager.GetTenantByKey(tenantKey), this.GetRequestUri());
        }

        /// <summary>
        /// This method saves 1 tenant given in the body of the request and create a response and template db for that tenant
        /// </summary>
        /// <param name="tenant">The tenant to be saved</param>
        /// <returns>The tenant that was saved</returns>
        // POST api/tenants
        [SwaggerResponse(System.Net.HttpStatusCode.Created, Description = "Tenant was successfully created")]
        [HttpPost]
        public IActionResult Post([FromBody][Required]Tenant<ConfigT> tenant)
        {
            // TODO: refactor this to use a Validator (that gets DI'ed), Also has to be within the lambda function of the ResponseFactory, else it doesn't fall within the try catch it provides
            var missingParams = new List<MissingParam>();
            if (tenant == null)
                missingParams.Add(new MissingParam("tenant", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);
            // END TODO

            return _responseFactory.CreatePostResponse(() => _tenantManager.InsertTenant(tenant), this.GetRequestUri(), "GetTenantById", new { controller = "Tenant", tenantkey = tenant.Id });
        }
    }
}
