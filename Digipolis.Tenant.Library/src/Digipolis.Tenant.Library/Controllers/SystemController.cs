﻿using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Status.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using Swashbuckle.SwaggerGen.Annotations;

namespace Digipolis.Tenant.Library.Controllers
{
    [Route("api/[controller]/status")]
    public class SystemController :  Controller
    {

        private IAdminSystemStatusManager _systemStatusManager;
        private IResponseFactory _responseFactory;

        public SystemController(IAdminSystemStatusManager systemStatusManager, IResponseFactory responseFactory)
        {
            _systemStatusManager = systemStatusManager;
            _responseFactory = responseFactory;
        }
        /// <summary>
        /// This method returns the status of the admin api
        /// </summary>
        /// <returns>A system status admin api</returns>
        // GET api/system/status
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(Response<SystemStatus>))]
        [HttpGet()]
        public IActionResult Get()
        {
            return _responseFactory.CreateGetResponse(() => _systemStatusManager.GetStatus(), this.GetRequestUri());
        }
    }
}
