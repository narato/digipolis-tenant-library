﻿using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Status.Models;
using Digipolis.Tenant.Library.DataProviders;
using System;

namespace Digipolis.Tenant.Library.Managers
{
    public class SystemStatusAdminManager : IAdminSystemStatusManager
    {

        private ITenantDataProvider _tenantDataProvider;

        public SystemStatusAdminManager(ITenantDataProvider tenantDataProvider)
        {
            _tenantDataProvider = tenantDataProvider;
        }

        public SystemStatus GetStatus()
        {
             var systemStatus = new SystemStatus(Guid.NewGuid(), "Form Engine Template API", "", "V1.0", "Development", DateTime.Now) { Up = true };
            
             var dbConnectionCheckResult = _tenantDataProvider.CheckDBConnection();

                if (!dbConnectionCheckResult.ConnectionOk)
                {
                    systemStatus.Up = false;
                    systemStatus.Components.Add(
                        new Component()
                        {
                            Name = "Postgres Database",
                            Description = $"The admin database for the form engine'",
                            Up = false,
                            MoreInfo = $"Connection to the Admin database failed. Reason: {dbConnectionCheckResult.Exception.Message};"
                        });
                }
            
            return systemStatus;
        }
    }
}
