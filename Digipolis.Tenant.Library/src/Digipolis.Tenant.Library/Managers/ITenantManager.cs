﻿using Digipolis.Common.Tenancy.MappedModels;

namespace Digipolis.Tenant.Library.Managers
{
    public interface ITenantManager<ConfigT>
    {
        Tenant<ConfigT> GetTenantByKey(string tenantKey);
        Tenant<ConfigT> InsertTenant(Tenant<ConfigT> tenant);
    }
}
