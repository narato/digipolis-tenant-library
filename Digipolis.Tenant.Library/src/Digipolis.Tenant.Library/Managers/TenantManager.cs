﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Mappers;
using Digipolis.Tenant.Library.ActionExecutors;
using Digipolis.Tenant.Library.DataProviders;

namespace Digipolis.Tenant.Library.Managers
{
    public class TenantManager<ConfigT> : ITenantManager<ConfigT>
    {
        private IActionExecutor<ConfigT> _actionExecutor;
        private ITenantDataProvider _tenantDataProvider;
        private IConfigMapper<ConfigT> _mapper;

        public TenantManager(ITenantDataProvider tenantDataProvider, IActionExecutor<ConfigT> executor, IConfigMapper<ConfigT> mapper)
        {
            _tenantDataProvider = tenantDataProvider;
            _actionExecutor = executor;
            _mapper = mapper;
        }
        public Tenant<ConfigT> GetTenantByKey(string key)
        {
            var tenant = _tenantDataProvider.GetTenantByKey(key);
            if (tenant == null)
                return null;
            return _mapper.Map(tenant);
            
        }

        public Tenant<ConfigT> InsertTenant(Tenant<ConfigT> tenant)
        {
            _tenantDataProvider.EnsureTenantKeyAvailable(tenant.Key);
            _actionExecutor.EnsureDbNamesAvailable(tenant);
            var insertedTenant = _tenantDataProvider.InsertTenant(_mapper.Map(tenant));
            var mappedTenant = _mapper.Map(insertedTenant);
            _actionExecutor.OnTenantCreated(tenant);

            return mappedTenant;
        }
    }
}
