﻿using Digipolis.Tenant.Library.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace Digipolis.Tenant.Library.ContextFactories
{
    public class TenantContextFactory : IDbContextFactory<TenantDbContext>
    {
        public TenantDbContext Create()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("config.json");
            var config = builder.Build();

            //Gets the Admin Api Endpoint from the config.json file
            var engineConfigSection = config.GetSection("EngineConfiguration");
            var connectionString = engineConfigSection.GetValue<string>("ConnectionString");

            return new TenantDbContext(connectionString);
        }
    }
}
